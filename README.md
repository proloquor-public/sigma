# sigma

[Sigma](https://github.com/SigmaHQ/sigma) is a generic signature format for Security Incident and Event Managment (SIEM) systems.  Sigma is for log files what Snort is for network traffic and YARA is for files.

This project creates a basic environment for building and running the Sigma tools.  You will need to have [docker](https://www.docker.com/) installed and configured on your local system.  

## Getting started

To build and run this project, first clone this project to a convenient directory (say, C:\sigma), then:

```
C:\> cd C:\sigma
C:\sigma> docker build -t sigma .
C:\sigma> docker run -it --rm -v $PWD\root:/root sigma
```

If it all worked, you should be presented with a linux prompt.  Run the test script:
```
root@bb20a80c4822:/# cd
root@bb20a80c4822:~# ./testSigma.sh
```
## Working with Sigma Signatures
For more information on Sigma, consult the [documentation](https://github.com/SigmaHQ/sigma).
