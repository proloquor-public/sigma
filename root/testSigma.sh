#!/bin/bash

sigma list targets
sigma list pipelines
sigma list formats splunk
sigma list formats elasticsearch

echo
echo "Sigma rule to look for suspcious execution paths..."
echo
cat /opt/sigma/rules/windows/process_creation/proc_creation_win_susp_execution_path.yml

echo
echo "Generating proc_creation_win_susp_execution_path.yml rule for Splunk..."
echo
sigma convert \
	--target splunk \
	--pipeline splunk_cim \
	--format default \
	/opt/sigma/rules/windows/process_creation/proc_creation_win_susp_execution_path.yml

echo
echo "Generating proc_creation_win_susp_execution_path.yml rule for Elasticsearch (default)..."
echo
sigma convert \
	--target elasticsearch \
	--pipeline ecs_windows \
	--format default \
	/opt/sigma/rules/windows/process_creation/proc_creation_win_susp_execution_path.yml

echo
echo "Generating proc_creation_win_susp_execution_path.yml rule for Elasticsearch (lucene)..."
echo
sigma convert -j3 \
	--target elasticsearch \
	--pipeline ecs_windows \
	--format dsl_lucene \
	/opt/sigma/rules/windows/process_creation/proc_creation_win_susp_execution_path.yml

