FROM python:3

RUN apt-get update
RUN apt-get install -y vim
RUN git clone https://github.com/SigmaHQ/sigma.git /opt/sigma
RUN pip install --upgrade pip
RUN pip install sigma-cli pysigma-backend-splunk pysigma-backend-elasticsearch pysigma-pipeline-sysmon

CMD ["bash"]
